package big.task;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;


public class BigTask {

  private String test;
  private String[] testArr;
  private String[] testArrWord;
  private List<String> testListWords;

  public BigTask(String test) {
    this.test = test;
    this.testArr = new String[test.length()];
    this.testArrWord = new String[test.length()];
    this.testListWords = new ArrayList<>();
  }


  private void splitOnSentences() {
    String example = "[.?!;]";
    testArr = test.split(example);
  }

  public void splitOnWords() {
    String pattern = "[\\W]";
    testArrWord = test.split(pattern);
    for (String s : testArrWord) {
      s = s.replaceAll("[\\W]", "");
      testListWords.add(s);
    }
    System.out.println(testListWords);
    for (int i = 0; i < testListWords.size(); i++) {
      if (testListWords.get(i) == ",") {
        System.out.println(testListWords.get(i));
        testListWords.remove(i);
      }
    }
    System.out.println(testListWords);

  }

  public void printSentenceInASCOrder() {
    splitOnSentences();
    Arrays.sort(testArr, Collections.reverseOrder());

    for (String s : testArr) {
      System.out.println(s);
    }
  }

  public void findWordInFirstSentenceThatOnlyExistsInIt() {
    String patternToCut = "[ ]";
    String patternToReplace = "[^A-Za-zА-Яа-я0-9]";
    String[] firstSentence;

    firstSentence = testArr[0].split(patternToCut);
    for (int i = 0; i < firstSentence.length; i++) {
      firstSentence[i] = firstSentence[i].replaceAll(patternToReplace, " ");
    }
    find(firstSentence);
  }

  private void find(String[] arrString) {
    for (String s : arrString) {
      for (int i = 1; i < testArr.length; i++) {
        if (s == arrString[i]) {
          i++;
          System.out.println("Found");
        } else {
          System.out.println(testArr[i]);
        }
      }
    }
  }

  public void askQuestions() {
    String pattern = "([A-ZА-Я][^.!?]*)\\?";
    String[] matches = Pattern.compile(pattern)
        .matcher(test)
        .results()
        .map(MatchResult::group)
        .toArray(String[]::new);
    int length = 4;
    for (String s : matches) {
      s = s.replaceAll("[?]", "");
      if (s.length() == length) {
        System.out.println(s);
      }
    }
  }

  public void findGlasniWords() {
    String[] matches = Pattern.compile("(?i)\\b[aeiou][^\\s]*(?=(\\s|$))")
        .matcher(test)
        .results()
        .map(MatchResult::group)
        .toArray(String[]::new);

  }

  public String[] findPrigolosWords() {
    String[] matches = Pattern.compile("(?i)\\b[^aeiou][^\\s]*(?=(\\s|$))")
        .matcher(test)
        .results()
        .map(MatchResult::group)
        .toArray(String[]::new);

    return matches;

  }

  private String findBiggestSentence() {
    splitOnWords();
    return Arrays.stream(testArr[0].split(" ")).max(Comparator.comparingInt(String::length))
        .orElse(null);

  }


  public String[] deletePrigolosniWords(int length) {
    String[] matches = findPrigolosWords();
    List<String> matchesList = new ArrayList<>();
    for (String s : matches) {
      s = s.replaceAll("[\\W]", "");
      matchesList.add(s);
    }

    for (int i = 0; i < matchesList.size(); i++) {
      if (matchesList.get(i).length() == length) {
        matchesList.remove(i);
      }
    }

    System.out.println(matchesList);
    return matches;

  }


}



